const { exec } = require('child_process');
const es = require('./elasticsearch.js');
const fs = require('fs');
const os = require('os');

const utils = require('./utils.js')

const root_config = `
#!/usr/local/sbin/tac_plus
id = spawnd {
    listen = { address = 0.0.0.0 port = 49 }
    spawn = {
            instances min = 1
            instances max = 10
    }
    background = no
}

id = tac_plus {
    debug = MAVIS

    access log = /var/log/tac_plus/access/%Y/%m/access-%m-%d-%Y.txt
    accounting log = /var/log/tac_plus/accounting/%Y/%m/accounting-%m-%d-%Y.txt
    authentication log = /var/log/tac_plus/authentication/%Y/%m/authentication-%m-%d-%Y.txt

    mavis module = external {
        script out = {
            if (undef($TACMEMBER) && $RESULT == ACK) set $RESULT = NAK
            if ($RESULT == ACK) set $PASSWORD_ONESHOT = 1
        }
        setenv LDAP_SERVER_TYPE = "`+ utils.env('LDAP_SERVER_TYPE', 'microsoft') + `"
        setenv LDAP_HOSTS = "`+ utils.env('LDAP_HOSTS', 'ldap://localhost') + `"
        setenv LDAP_BASE = "`+ utils.env('LDAP_BASE', 'DC=my,DC=domain') + `"
        setenv LDAP_SCOPE = "`+ utils.env('LDAP_SCOPE', 'sub') + `"
        setenv LDAP_FILTER = "`+ utils.env('LDAP_FILTER', '(&(objectClass=user)(objectClass=person)(sAMAccountName=%s))') + `"
        setenv LDAP_USER = "`+ utils.env('LDAP_USER', 'admin') + `"
        setenv LDAP_PASSWD = "`+ utils.env('LDAP_PASSWD', 'admin') + `"
        setenv AD_GROUP_PREFIX = "`+ utils.env('AD_GROUP_PREFIX', '') + `"
        setenv UNLIMIT_AD_GROUP_MEMBERSHIP = "1"
        setenv EXPAND_AD_GROUP_MEMBERSHIP = 0
        setenv REQUIRE_TACACS_GROUP_PREFIX = 0
        setenv FLAG_USE_MEMBEROF = 1
        exec = /usr/local/lib/mavis/mavis_tacplus_ldap.pl
    }

    login backend = mavis
    user backend = mavis
    connection timeout = 600
    context timeout = 3600
    password max-attempts = 1
    password backoff = 1
    separation tag = "*"
    skip conflicting groups = yes
    skip missing groups = yes
    user backend = mavis
    login backend = mavis chpass
    pap backend = mavis

    # Global host group.
    host = world {
        address = 0.0.0.0/0
        prompt = "`+ utils.env('LOGIN_PROMPT', 'Welcome! Please login to continue.') + `"
        key = "`+ utils.env('TAC_KEY', 'supersafe') + `"
    }
    {{host_list}}
}
`;

function parse_array(array) {
    if (array.includes(",")) {
        array = array.split(",")
    } else {
        array = [array]
    }
    return array;
}

function write_config(callback) {
    console.log("[i] TACACS: Building new configuration.");
    es.settings_get("tacacs_groups", groups => {
        es.settings_get("tacacs_super_groups", super_groups => {
            es.settings_get("tacacs_read_all_groups", read_all_groups => {
                super_groups = parse_array(super_groups);
                groups = parse_array(groups);
                if (read_all_groups) {
                    read_all_groups = parse_array(read_all_groups);
                }                
                es.device_get("*", function (host_list) {
                    var dynamic_config = `
                    # Network Host Definitions.`;
                    host_list.forEach(host => {
                        if (!dynamic_config.includes(host.ip.trim())){
                            dynamic_config += `
                            host = `+ host.name.trim() + ` {
                                address = "`+ host.ip.trim() + `"
                                template = world
                            }
                            `;
                        }                    
                    });

                    var group_config = `
                    # Group Access Permissions.`;

                    if (read_all_groups) {
                        read_all_groups.forEach(group => {
                    group_config += `
                    group = `+ group.trim() +` {
                        message = "Welcome! Your access mode is set to 'Read-Only'."`;
                    group_config += `
                        server = permit 0.0.0.0/0`;

                    group_config += `
                        default service = permit
                        # IOS
                        service = shell {
                            set priv-lvl = 15
                            default attribute = permit
                            cmd = show {
                                permit .* # default permit
                            }
                        }
                        # WLC
                        service = ciscowlc {
                            set role1 = MONITOR
                        }
                        # Fortigate
                        service = fortigate {
                            optional admin_prof = read_only
                        }
                    }`;
                        });                        
                    }                    

                    super_groups.forEach(group => {
                        group_config += `
                    group = `+ group.trim() + ` {
                        message = "Welcome SuperUser! Your access mode is set to 'Read-Write'."`;
                        host_list.forEach(host => {
                            if (true) {
                                if(dynamic_config.includes(host.name.trim())){
                                    group_config += `
                        server = permit `+ host.name.trim() + ``;
                                }                                
                            }
                        });
                        group_config += `
                        server = deny 0.0.0.0/0`;

                        group_config += `
                        default service = permit
                        # IOS
                        service = shell {
                            set priv-lvl = 15
                            default attribute = permit
                            default cmd = permit
                        }
                        # WLC
                        service = ciscowlc {
                            set role1 = ALL
                        }
                        # Fortigate
                        service = fortigate {
                            optional admin_prof = super_admin
                        }
                    }`;
                    });

                    groups.forEach(group => {
                        group_config += `
                    group = `+ group.trim() + ` {
                        message = "Welcome! Your access mode is set to 'Read-Write'."`;
                        host_list.forEach(host => {
                            if (host.group.trim() === group.trim()) {
                                if(dynamic_config.includes(host.name.trim())){
                                    group_config += `
                        server = permit `+ host.name.trim() + ``;
                                }
                            }
                        });
                        group_config += `
                        server = deny 0.0.0.0/0`;
                        
                        group_config += `
                        default service = permit
                        # IOS
                        service = shell {
                            set priv-lvl = 15
                            default attribute = permit
                            default cmd = permit
                        }
                        # WLC
                        service = ciscowlc {
                            set role1 = ALL
                        }
                        # Fortigate
                        service = fortigate {
                            optional admin_prof = super_admin
                        }
                    }
                    `;
                    });

                    dynamic_config += group_config;
                    var out_config = root_config.replace("{{host_list}}", dynamic_config);
                    fs.writeFile("/usr/local/etc/tac_plus_new.cfg", out_config, function (err) {
                        callback(true);
                    });
                });
            });//END
        });
    });
}

var attempts = 0;
function build_config(callback = null) {
    if (attempts < 3) {
        attempts++;
        write_config(success => {
            if (success) {
                verify_config(config_valid => {
                    if (config_valid) {
                        apply_config(config_applied => {
                            if (config_applied) {
                                status(is_running => {
                                    if (is_running) {
                                        restart(restart_success => {
                                            if (restart_success) {
                                                restart(restart_success => {
                                                    if (callback) {
                                                        callback(true);
                                                    }
                                                });
                                            } else {
                                                build_config(callback);
                                            }
                                        });
                                    } else {
                                        start(result => {
                                            if (result) {
                                                if (callback) {
                                                    callback(true);
                                                }
                                            } else {
                                                build_config(callback);
                                            }
                                        });
                                    }
                                });
                            } else {
                                console.log("[e] Failed to apply config, attempt: " + attempts);
                                build_config(callback);
                            }
                        });
                    } else {
                        console.log("[e] Failed to verify config, attempt: " + attempts);
                        build_config(callback);
                    }
                });
            } else {
                console.log("[e] Failed to build config, attempt: " + attempts);
                build_config(callback);
            }
        });
    }
}

var checker = null;

function is_internal_trigger(callback) {
    es.settings_get("last_tac_change_hostname", value => {
        if (value == os.hostname()) {
            callback(true);
        } else {
            callback(false);
        }
    });
}

function non_api_config(task) {
    console.log("[i] Non API task completed.");
}

var trigger_change_in_progress = false;
function tacacs_monitor() {
    if (checker == null) {
        console.log("[c] TACACS: Cluster monitor started!");
        checker = setInterval(tacacs_monitor, 5000);
    }
    es.settings_get("last_tac_change", value => {
        try {
            var last_change = (Date.now() - parseInt(value)) / 1000;
            if (last_change <= 10 && !trigger_change_in_progress) {
                is_internal_trigger(result => {
                    if (!result) {
                        es.settings_get("last_tac_change_action", action => {
                            console.log("[c] TACACS: Task '" + action + "' invoked. Requested " + last_change + "s ago.");
                            trigger_change_in_progress = true;
                            switch (action) {
                                case "configure":
                                    build_config(non_api_config);
                                    break;
                                case "start":
                                    start(non_api_config);
                                    break;
                                case "stop":
                                    stop(non_api_config);
                                    break;
                                case "restart":
                                    restart(non_api_config);
                                    break;
                                default:
                                    console.log("[i] Task not recognised '" + action + "'");
                                    break;
                            }
                        });
                    }
                });
            } else {
                trigger_change_in_progress = false;
            }
        } catch (err) {
            console.log("[e] " + err);
            // Nothing.
        }
    });
}

function verify_config(callback = null) {
    exec(`/usr/local/sbin/tac_plus -P /usr/local/etc/tac_plus_new.cfg`, (err, stdout, stderr) => {
        if (err) {
            if (callback) {
                callback(false);
            }
        } else {
            if (callback) {
                callback(true);
            }
        }
    });
}

function apply_config(callback = null) {
    fs.rename("/usr/local/etc/tac_plus_new.cfg", "/usr/local/etc/tac_plus.cfg", err => {
        if (err) {
            if (callback) {
                callback(false);
            }
        } else {
            console.log("[i] TACACS: New configuration applied.");
            if (callback) {
                callback(true);
            }
        }
    })
}

function status(callback = null) {
    exec('service tac_plus status', { timeout: 10000 }, (err, stdout, stderr) => {
        if (err || stderr) {
            if (callback) {
                callback(false);
            }
        } else {
            if (callback) {
                callback(true);
            }
        }
    });
}

function restart(callback = null) {
    exec('service tac_plus restart', options = { timeout: 15000 }, (err, stdout, stderr) => {
        stop(stop_result => {
            start(start_result => {
                if (callback) {
                    callback(start_result);
                }
            });
        });
    });
}

function start(callback = null) {
    exec('service tac_plus start', options = { timeout: 10000 }, (err, stdout, stderr) => {
        setTimeout(function () {
            status(
                result => {
                    if (callback) {
                        callback(result);
                    }
                }
            )
        }, 1000);
    });
}

function stop(callback = null) {
    exec('service tac_plus stop', options = { timeout: 10000 }, (err, stdout, stderr) => {
        setTimeout(function () {
            status(
                result => {
                    if (callback) {
                        callback(!result);
                    }
                }
            )
        }, 1000);
    });
}


var startup = true;
tacacs_monitor();

module.exports = {
    cat: function (callback) {
        fs.readFile("/usr/local/etc/tac_plus.cfg", data => {
            callback(data);
        });
    },
    access: function (callback = null) {
        exec(`find /var/log/tac_plus/access | grep .txt | xargs cat`, (err, stdout, stderr) => {
            callback(stdout);
        });
    },
    accounting: function (callback = null) {
        exec(`find /var/log/tac_plus/accounting | grep .txt | xargs cat`, (err, stdout, stderr) => {
            callback(stdout);
        });
    },
    authentication: function (callback = null) {
        exec(`find /var/log/tac_plus/authentication | grep .txt | xargs cat`, (err, stdout, stderr) => {
            callback(stdout);
        });
    },
    configure: function (callback = null) {
        if (!startup) {
            es.settings_put("last_tac_change", (Date.now()), done => {
                es.settings_put("last_tac_change_hostname", (os.hostname()), done => {
                    es.settings_put("last_tac_change_action", "configure", done => {
                        console.log("[i] Registered 'configure' task.");
                        build_config(callback);
                    });
                });
            });
        } else {
            startup = false;
            build_config(callback);
        }

    },
    status: function (callback = null) {
        if (callback) {
            status(callback);
        }
    },
    restart: function (callback = null) {
        es.settings_put("last_tac_change", (Date.now()), done => {
            es.settings_put("last_tac_change_hostname", (os.hostname()), done => {
                es.settings_put("last_tac_change_action", "restart", done => {
                    console.log("[i] Registered 'restart' task.");
                    restart(callback);
                });
            });
        });
    },
    start: function (callback = null) {
        es.settings_put("last_tac_change", (Date.now()), done => {
            es.settings_put("last_tac_change_hostname", (os.hostname()), done => {
                es.settings_put("last_tac_change_action", "start", done => {
                    console.log("[i] Registered 'start' task.");
                    start(callback);
                });
            });
        });
    },
    stop: function (callback = null) {
        es.settings_put("last_tac_change", (Date.now()), done => {
            es.settings_put("last_tac_change_hostname", (os.hostname()), done => {
                es.settings_put("last_tac_change_action", "stop", done => {
                    console.log("[i] Registered 'stop' task.");
                    stop(callback);
                });
            });
        });
    },
};