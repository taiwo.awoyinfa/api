const express = require('express');
var bodyParser = require("body-parser");
var cors = require('cors');
const app = express();
const es = require("../elasticsearch.js");
const tacacs = require("../tacacs.js");
const ansible = require("../ansible.js");
const utils = require("../utils.js");
const diff = require('git-diff');

const basic_auth = require('express-basic-auth');

const admin_username = utils.env("API_ADMIN_USER", "admin");
const admin_password = utils.env("API_ADMIN_PASSWORD", "admin");
app.use(cors());
app.use(basic_auth({
    users: { 
        [admin_username] : admin_password
    },
    unauthorizedResponse: {
        401: "Access denied. This resource is protected."
    }
}))

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", function (req, res){
    res.redirect("/api");
});
app.get('/api', function (req, res) {
    res.sendFile(__dirname + "/api.html");
});

app.get('/api/endpoints', function (req, res) {
    res.send({
        "methods": [
            "GET"
        ],
        "GET": [
            "/api/devices",
            "/api/devices/<option>",
            "/api/devices/<option>/scrape",
            "/api/credentials",
            "/api/credentials/<option>",
        ]
    });
});

app.get('/api/devices', function (req, res) {
    es.device_get("*", function(data){
        res.send(data);
    });
});

app.get('/api/modules', function (req, res) {
    res.send(utils.installed_modules());
});

app.get('/api/devices/*/scan', function (req, res) {
    var parts = req.path.split('/');
    ansible.scrape(parts[parts.length - 2], function(resp) {
        res.send(resp);
    });
});

app.get('/api/devices/*/config/diff', function (req, res) {
    var parts = req.path.split('/');
    console.log("[i] Diff search requested for " + parts[parts.length - 3])
    es.config_get(parts[parts.length - 3], function(data){
        var diff_list = [];
        if (data && data.length > 1){
            for (var i = 1; i < data.length; i++) {
                var diff_result = diff(data[i].config, data[i-1].config, {flags: '--diff-algorithm=minimal --ignore-all-space --unified=1'});
                if (diff_result === undefined) {
                    console.log("[i] No diff, just adding config.")
                    diff_result = data[i].config;
                }
                lines = diff_result.split(/\r\n|\r|\n/);
                lines_modified = 0;
                for (var j = 0; j < lines.length; j++) {
                    lines[j] = lines[j].trim();
                    if (lines[j].startsWith("+") || lines[j].startsWith("-")){
                        lines_modified ++;
                    }
                }
                diff_list.push({
                    result: diff_result,
                    changes: lines_modified,
                    config_1: data[i-1].timestamp,
                    config_2: data[i].timestamp
                })
            }
            res.send(diff_list);
        }else{
            res.send(
                {
                    success: false,
                    msg: "Looks like there isn't enough data to perform a diff."
                }
            );
        }
    });
});

app.get('/api/devices/*/config', function (req, res) {
    var parts = req.path.split('/');
    console.log("[i] Config search requested for " + parts[parts.length - 2])
    es.config_get(parts[parts.length - 2], function(data){
        res.send(data);
    });
});

app.delete('/api/devices/*/config', function (req, res) {
    var parts = req.path.split('/');
    es.config_delete(parts[parts.length - 2], function(data){
        res.send(data);
    });
});

app.post('/api/devices/*', function (req, res) {
    var parts = req.path.split('/');
    console.log("[i] Adding device " + parts[parts.length - 1]);
    if (req.body.group){
        console.log("[i] Taggin device with group " + req.body.group);
    }else {
        req.body.group = "default";
    }
    es.device_put(parts[parts.length - 1], req.body.ip, req.body.make, {}, req.body.type, req.body.credential, req.body.group);
    res.send(
        {
            success: true,
            msg: "Device '" + parts[parts.length - 1] + "' saved."
        }
    )
});

app.delete('/api/devices/*', function (req, res) {
    var parts = req.path.split('/');
    res.send({
        success: es.device_delete(parts[parts.length - 1])
    });
});

app.get('/api/devices/*', function (req, res) {
    var parts = req.path.split('/');
    es.device_get(parts[parts.length - 1], function(data){
        res.send(data);
    });
});

app.get('/api/credentials', function (req, res) {
    es.credentials_get("*", function(data){
        res.send(data);
    });
});

app.delete('/api/credentials/*', function (req, res) {
    var parts = req.path.split('/');
    res.send({
        success: es.credentials_delete(parts[parts.length - 1])
    });
});

app.post('/api/credentials/*', function (req, res) {
    var parts = req.path.split('/');
    try{
        es.credentials_put(parts[parts.length - 1], req.body.username, req.body.password);
        res.send(
            {
                success: true,
                msg: "Credential '" + parts[parts.length - 1] + "' saved."
            }
        )
    } catch(err){
        res.send(
            {
                success: false,
                msg: err
            }
        )
    }    
});

app.get('/api/credentials/*', function (req, res) {
    var parts = req.path.split('/');
    es.credentials_get(parts[parts.length - 1], function(data){
        res.send(data);
    });
});

app.post( '/api/groups/default', function (req, res) {
    res.send("Coming soon.");
});

app.post( '/api/groups/super', function (req, res) {
    res.send("Coming soon.");
});

app.post( '/api/groups/general', function (req, res) {
    res.send("Coming soon.");
});

app.get('/api/tacacs/status', function(req, res) {
    tacacs.status(status => {
        res.send(status);
    })
});

app.get('/api/tacacs/start', function(req, res) {
    tacacs.start(status => {
        res.send(status);
    })
});

app.get('/api/tacacs/stop', function(req, res) {
    tacacs.stop(status => {
        res.send(status);
    })
});

app.get('/api/tacacs/restart', function(req, res) {
    tacacs.restart(status => {
        res.send(status);
    })
});

app.get('/api/tacacs/configure', function(req, res) {
    tacacs.configure(status => {
        res.send(status);
    })
});

app.post("*", function (req, res){
    res.status(405);
});

app.use(express.static(__dirname + "/static"));
const listen_port = utils.env("WEBSERVER_PORT", 5000);

module.exports = {
    // Night Owl Settings and logging.
    initialize: function() {
        app.listen(listen_port);
        console.log("[i] Listening on http://localhost:" + listen_port);
    },
}