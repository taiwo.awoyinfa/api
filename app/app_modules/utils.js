const fs = require('fs');

function log (log_string) {
    console.log("");
}

function get_env (var_name, default_var) {
    if (process.env[var_name] != null || process.env[var_name] != undefined){
        return process.env[var_name];
    }
    return default_var;    
}
function get_installed_modules () {
    var modules = [];
    var module_dir = __dirname;
    module_dir = module_dir.replace(module_dir.split("/")[module_dir.split("/").length -1], "ansible-playbooks");
    console.log("[i] Scanning for installed modules...");
    fs.readdir(module_dir, (err, files) => {
        for(var i = 0; i < files.length; i++) {
            var file = files[i].replace(".yaml", "").replace(".yml", "");
            if (file != "add_host") {
                modules.push(file);
            }
        }
        return modules;
    });
}
module.exports = {
    env: function(var_name, default_var) {
        return get_env(var_name, default_var);
    },
    installed_modules: function() {
        return get_env(var_name, default_var);
    },
}