console.log("[i] Night Owl " + process.env.npm_package_version + ", startup initialized...");

console.log("[i] Starting elasticsearch module...");
const es = require(__dirname + "/app_modules/elasticsearch");
es.initialize();

const logger = require(__dirname + "/app_modules/log_collector");
logger.initialize();

es.settings_get("tacacs_enabled", result => {
    if (result) {
        console.log("[i] Starting TACACS module...");
        const tacacs = require("./app_modules/tacacs")
        tacacs.configure(
            result => {
                tacacs.status(is_running => {
                    if(is_running){
                        console.log("[i] TACACS module Started.");
                    }else{
                        console.log("[e] TACACS module Startup Failed.");
                    }
                })                
            }
        )
    }
    start_web();
})

function start_web () {
    console.log("[i] Starting webserver module...");
    const ws = require("./app_modules/web/webserver")
    ws.initialize();
    console.log("[i] Startup Complete.");
}