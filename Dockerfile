FROM ubuntu:bionic
WORKDIR /root

# Install base packages from Ubuntu and Ansible PPAs
RUN apt-get update && apt-get install nodejs npm python python-pip sshpass openssh-client bash software-properties-common build-essential libnet-ldap-perl libpcre3-dev wget -y
RUN apt-add-repository --yes --update ppa:ansible/ansible
RUN apt-get install ansible -y

# Install TACPLUS and Mavis.
ADD tacplus/install /root/install
RUN chmod +x install
RUN ./install

# Install TACPLUS Service.
RUN cp ~/PROJECTS/tac_plus/extra/etc_init.d_tac_plus /etc/init.d/tac_plus
RUN chmod 755 /etc/init.d/tac_plus
RUN chown root:root /etc/init.d/tac_plus
RUN update-rc.d tac_plus defaults
RUN cd /usr/local/etc; touch tac_plus.cfg; chmod 755 tac_plus.cfg;

# Add startup script.
ADD scripts/startup /startup
RUN chmod +x /startup

# Upgrade NPM to the latest version.
RUN npm install npm --upgrade -g

# Copy API code and install required 3rd party modules.
ADD app/ .
RUN npm install

# Declare port and entrypoint.
EXPOSE 5000 49
ENTRYPOINT ["bash", "/startup"]