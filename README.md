## Night Owl API Documentation
## What is Night Owl?
Night owl is aimed at providing key features designed to aid and improve processes related to management of network hardware. When fully developed, the project aims to deliver the following:
#### Configuration Management
- **Scrape**: Automatically monitor live configurations on devices and provide a diff overview of any changes between scrapes.
- **Configure**: Allow the execution of scripts on devices, either targeting a single switch/firewall/wireless land controller, or a group of devices of the same category.
- **Alert**: Automatiocally set-up alearts if a device configuration is changed. This can either be the entire configuration os sections of it, based on include/exclude rules.
#### Tacacs
The API component of Night Owl is capable of running as a Tacacs server based on TacPlus. It uses the Mavis module to provide integrated Active Directory authentication and group based access control, as well as 3 levels of account types:
 - **superusers**: r/w access to all devices.
 - **owners**: r/w access to selected devices.
 - **readers**: r/o access to all devices.
 - **conditional readers**: r/o access to selected devices.

## Notes
Please note, this project is in early stages of development and is still missing key components, especially in regards to initial setup of the elasticsearch data. If you are interested stay tuned, and feel free to fork and submit a merge request if you would like to contribute.

Contribution guidelines are available here: [https://gitlab.com/night-owl/api/blob/master/CONTRIBUTING.md](https://gitlab.com/night-owl/api/blob/master/CONTRIBUTING.md)
## Components
#### API
Node.js api providing core interaction with the application.
#### TacPlus
TacPlus service, monitored and managed by the Node.js API.
#### Ansible
The API uses ansible to perform all direct interactions with devices, this ensures wide support and standardised components and capabilities across multiple vendors and software versions.
#### Elasticsearch
Used as the storage back-end for Night Owl. This provides high performance searches when scanning/comparing device configrations and should ensure reliable use even with large sets of devices and a long history of configurations.

## Endpoints
#### GET
- `/api`
    - `/endpoints`
        - `/`: list avaliable endpoints and methods.
    - `/devices`
        - `/`: list all registered devices.
        - `/<device-name>`
            - `/`: list all details of a specified device.
            - `/scan`: scan device for it's current configuration and additional details such as model and serial number.
            - `/config`
                - `/`: show all configurations saved for the given device.
                - `/diff`: process and show git diff between all configurations.
    - `/credentials`
        - `/`: show all available credentail set.
        - `/<credential-name>`
            - `/`: decrypt and display the username:password combination for the given credential set.
    - `/tacacs`
        - `/`: show current tacacs configuration file.
        - `/start`: start the tacacs service.
        - `/stop`: stop the tacacs service.
        - `/restart`: restart the tacacs service.
        - `/configure`: build, verify and apply a new configuration based on registered devices and groups.

#### POST
- `/api`
    - `/devices`
        - `/<device-name>`
            - `/`: add a new device, structure of the payload body is detailed below.
    - `/credentials`
        - `/<credential-name>`
            - `/`: add a new credential set, structure of the payload body is detailed below.
#### DELETE
- `/api`
    - `/devices`
        - `/<device-name>`
            - `/`: delete the specified device.
    - `/credentials`
        - `/<credential-name>`
            - `/`: delete the specified credential.

## Request Examples
#### POST `/api/devices/GBL-SW-PRIMARY`
**Headers**
```
Content-Type: application/json
Authorization: Basic YWRtaW46c3VwZXJzYWZlCg==
```
**Body**
```
{
    "ip": "1.2.3.4",
    "make": "cisco"
    "type": "ios",
    "credential": "default",
    "group": "default"
}
```
**Body Details**
- `ip`: management ip address of the device, reachable from night owl.
- `make`: manufacturer of the product, currently supported values are:
    - `cisco`
    - `dell`
    - `fortinet`
- `type`: software type of the device, currently supported values are:
    - `ios`
    - `dellos`
    - `fortios`
- `credential`: the credential pair to be used to authenticate against the given device.
- `group`: the group that should be tagged on this device, required if you are using the tacacs functionality, otherwise can be left as "default".
# 
#### POST `/api/credentials/my-super-credential`
**Headers**
```
Content-Type: application/json
Authorization: Basic YWRtaW46c3VwZXJzYWZlCg==
```
**Body**
```
{
    "username": "admin-username",
    "password": "admin-super-secret"
}
```
**Body Details**
- `username`: username for the management R/W account used to interact with devices.
- `password`: password for the management R/W account used to interact with devices.


## Environment Variables
These variable are used to define parameters for night owl's correct operation.